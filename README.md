# botdiscord

## Camandos

###agregar un día sin ragnarok

```>add day```

retorna **1 día sin ragnarok**

###remover un día de ragnarok

``>remove day``

retorna **0 día sin ragnarok**

###agregar un fix activo

``>add fix``

retorna **1 fix activo**

###remover un fix activo

``>remove fix``

retorna **0 fix activo**

###reiniciar contador de días

``>reset day``

retorna **0 día sin ragnarok**

###reiniciar contador fix activo

``>reset fix``

retorna **0 fix activo**

### ver estado

``>status``

retorna **estado ragnarok**

**day: 2**

**fix: 1** 