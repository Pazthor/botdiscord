from discord.ext import commands
from dotenv import load_dotenv
from model.connect import Database
import os
load_dotenv()
userdb = os.environ["__USERDATABASE"]
password = os.environ["__PASSWORDDATABASE"]
server = os.environ["__SERVERIP"]
database = os.environ["__DATABASE"]
token = os.environ["__TOKENAPP"]
class Ragnarok(commands.Bot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db = Database(userdb,password,server,database)

    def value(self, server: str, tipo: str):
        try:
            values = self.db.getValues(server)
            print(values)
            if values[tipo]:
                return values[tipo]
            else:
                return 0

        except:
            return 0

    def insertValue(self, server: str, tipo: str,  number: int):
        valor= {}
        valor[tipo] = number
        # print(valor)
        self.db.insertValue(server, valor)

    def status(self, server):
        try:
            message = ""
            data = self.db.getValues(server)
            for key in data:
                if key == "_id":
                    message += "\n\nestado ragnarok"
                else:
                    message += "\n%s: %s"%(key, data[key])
            return message
        except:
            return "aún sin estado"
bot = Ragnarok(command_prefix='>', description="bot de ragnarok")

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

@bot.command(pass_context=True)
async def add(ctx, tipo=""):

    discordserv = ctx.message.guild.name
    number = bot.value(discordserv, tipo)
    if tipo == "day":
        bot.insertValue(discordserv, tipo, number + 1)
        valor = bot.value(discordserv, tipo)
        message = "%s %s" % (valor, "día sin ragnarok" if valor < 2 and valor >= 0 else "días sin ragnarok")
        await ctx.send(message)
        return
    if tipo == "fix":
        bot.insertValue(discordserv, tipo, number + 1)
        valor = bot.value(discordserv, tipo)
        message = "%s %s" % (valor, "fix activo" if valor < 2 and valor >= 0 else "fixes activos")
        await ctx.send(message)
        return

    await ctx.send('''
    debes poner [day] si quieres agregar un día sin ragnarok o [fix] para agregar un fix activo 
    ''')

@bot.command()
async def remove(ctx, tipo=""):
    discordserv = ctx.message.guild.name
    number = bot.value(discordserv, tipo)
    if tipo == "day":
        bot.insertValue(discordserv, tipo, number-1)
        valor = bot.value(discordserv, tipo)
        message = "%s %s"%(valor, "día sin ragnarok" if valor < 2 and valor >= 0 else "días sin ragnarok" )
        await ctx.send(message)
        return
    if tipo == "fix":
        bot.insertValue(discordserv, tipo, number - 1)
        valor = bot.value(discordserv, tipo)
        message = "%s %s" % (valor, "fix activo" if valor < 2 and valor >= 0 else "fixes activos")
        await ctx.send(message)
        return
    await ctx.send('''
        debes poner [day] si quieres quitar un día sin ragnarok o [fix] para quitar un fix activo 
        ''')

@bot.command()
async def reset(ctx, tipo=""):
    discordserv = ctx.message.guild.name
    if tipo == "day":
        bot.insertValue(discordserv, tipo, 0)
        valor = bot.value(discordserv, tipo)
        message = "%s %s"%(valor, "día sin ragnarok" if valor < 2 or valor > 0 else "días sin ragnarok" )
        await ctx.send(message)
    if tipo == "fix":
        bot.insertValue(discordserv, tipo, 0)
        valor = bot.value(discordserv, tipo)
        message = "%s %s" % (valor, "fix activo" if valor < 2 or valor > 0 else "fixes activos")
        await ctx.send(message)
    await ctx.send('''
            debes poner [day] si quieres reiniciar los días sin ragnarok o [fix] para reinciar los fixes activos 
            ''')


@bot.command()
async def status(ctx):
    discordserv = ctx.message.guild.name
    await ctx.send(bot.status(discordserv))


bot.run(token)

