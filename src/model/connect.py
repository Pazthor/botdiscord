from pymongo import MongoClient


class Database(object):

    def __init__(self, user: str, password: str, server: str, database: str):


        CONNECTION_STRING = "mongodb://%s:%s@%s/%s" % (user, password, server, database)
        self.client = MongoClient(CONNECTION_STRING)
        self.dbname = self.client['bots']

    def insertValue(self, server: str, values: dict):

        # self.dbname["bots"].replace_one({"_id": server}, values, upsert=True)
        self.dbname["bots"].update_one(
            {"_id": server},
            {"$set": values}
        )

    def getValues(self, server: str):
        return self.dbname["bots"].find_one({"_id": server})



# This is added so that many files can reuse the function get_database()
if __name__ == "__main__":
    # Get the database
    from dotenv import load_dotenv
    import os

    load_dotenv()
    user = os.environ["__USERDATABASE"]
    password = os.environ["__PASSWORDDATABASE"]
    server = os.environ["__SERVERIP"]
    database = os.environ["__DATABASE"]
    botdb = Database(user, password,server, database)
    botdb.insertValue("ragnarok", {"count": 23})
    result = botdb.getValues("ragnarok")
    print(result)
